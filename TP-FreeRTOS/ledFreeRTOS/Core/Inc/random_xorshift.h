/*
 * random_xorshift.h
 *
 *  Created on: 15 jun. 2020
 *      Author: lucio
 */

#ifndef INC_RANDOM_XORSHIFT_H_
#define INC_RANDOM_XORSHIFT_H_


unsigned long xorshift128(void);

#endif /* INC_RANDOM_XORSHIFT_H_ */
