/*
 * tareas.h
 *
 *  Created on: 15 jun. 2020
 *      Author: lucio
 */



#ifndef INC_TAREAS_H_
#define INC_TAREAS_H_

// Includes //
#include "main.h"
#include "cmsis_os.h"
#include "random_xorshift.h"

// Defines //

//GPIO

#define LED1 GPIOA,GPIO_PIN_3
#define LED2 GPIOA,GPIO_PIN_4
#define LED3 GPIOA,GPIO_PIN_5
#define BOTON GPIOA,GPIO_PIN_6


// ESTADOS

#define INIT 			0
#define T_L1 			1
#define T_L2 			2
#define T_L3 			3
#define T_LRANDOM 		4
#define CANT_ESTADOS 	4

// UMBRALES

#define ESTADOS_ESTABLES 10

// Prototipos //

void tarea_boton(void *p);
void tarea_prior(void *p);
void tarea_led1(void *p);
void tarea_led2(void *p);
void tarea_led3(void *p);

#endif /* INC_TAREAS_H_ */
