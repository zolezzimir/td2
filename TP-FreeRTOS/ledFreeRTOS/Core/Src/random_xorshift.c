/*
 * random_xorshift.c
 *
 *  Created on: 15 jun. 2020
 *      Author: lucio
 */


#include "random_xorshift.h"



unsigned long xorshift128(void){

	static unsigned long w=0x00168C55; // Legajo Lucio Zolezzi 14777717 en hexa
	static unsigned long x=0x5555AAAA,y=0x5555AAAA,z=0x5555AAAA;
	unsigned long t;
	t=(x^(x<<11));
	x=y;y=z;z=w;
	w=(w^(w>>19))^(t^(t>>8));
	return(w);
}
