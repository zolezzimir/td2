/*
 * tareas.c
 *
 *  Created on: 15 jun. 2020
 *      Author: lucio
 */


#include "tareas.h"

unsigned short flag_boton=0;	//Flag para indicar si se presiono el pulsador
unsigned short estado=INIT;


void tarea_prior(void *p)
{
	static unsigned random_val;

	while(1)
	{

		switch(estado)
		{
			case INIT:
				vTaskPrioritySet(tarea_led1, 1);
				vTaskPrioritySet(tarea_led2, 1);
				vTaskPrioritySet(tarea_led3, 1);
//				vTaskSuspend(tarea_prior);
				break;
			case T_L1:
				vTaskPrioritySet(tarea_led1, 2);
				vTaskPrioritySet(tarea_led2, 1);
				vTaskPrioritySet(tarea_led3, 1);
//				vTaskSuspend(tarea_prior);
				break;
			case T_L2:
				vTaskPrioritySet(tarea_led1, 1);
				vTaskPrioritySet(tarea_led2, 2);
				vTaskPrioritySet(tarea_led3, 1);
//				vTaskSuspend(tarea_prior);
				break;
			case T_L3:
				vTaskPrioritySet(tarea_led1, 1);
				vTaskPrioritySet(tarea_led2, 1);
				vTaskPrioritySet(tarea_led3, 2);
//				vTaskSuspend(tarea_prior);
				break;
			case T_LRANDOM:
				random_val=xorshift128();	//Genero numero aleatorio
				random_val&=(0x00000003);	//Tomo los dos bits del numero aleatorio
				switch(random_val)
				{
					case 1:
						vTaskPrioritySet(tarea_led1, 2);
						vTaskPrioritySet(tarea_led2, 1);
						vTaskPrioritySet(tarea_led3, 1);
//						vTaskSuspend(tarea_prior);
						break;
					case 2:
						vTaskPrioritySet(tarea_led1, 1);
						vTaskPrioritySet(tarea_led2, 2);
						vTaskPrioritySet(tarea_led3, 1);
//						vTaskSuspend(tarea_prior);
						break;
					case 3:
						vTaskPrioritySet(tarea_led1, 1);
						vTaskPrioritySet(tarea_led2, 1);
						vTaskPrioritySet(tarea_led3, 2);
//						vTaskSuspend(tarea_prior);
						break;
					default: //si el valor es 0 o por algun motivo se sale de rango, los pongo todos en 1
						vTaskPrioritySet(tarea_led1, 1);
						vTaskPrioritySet(tarea_led2, 1);
						vTaskPrioritySet(tarea_led3, 1);
//						vTaskSuspend(tarea_prior);
						break;
				}
				break;
			default:
				estado=0;
//				vTaskSuspend(tarea_prior);
		}
		vTaskDelay(20/portTICK_RATE_MS);
	}

}

void tarea_led1(void *p)
{
	while(1)
	{
		HAL_GPIO_TogglePin(LED1);
		vTaskDelay(1000/portTICK_RATE_MS);
	}

}
void tarea_led2(void *p)
{
	while(1)
	{
		HAL_GPIO_TogglePin(LED2);
		vTaskDelay(1000/portTICK_RATE_MS);
	}
}
void tarea_led3(void *p)
{
	while(1)
	{
		HAL_GPIO_TogglePin(LED3);
		vTaskDelay(1000/portTICK_RATE_MS);
	}

}

void tarea_boton(void *p)
{
	static unsigned long estados_estables=0, estado_anterior=0;
	unsigned long estado_actual=0;

	while(1)
	{

		estado_actual=HAL_GPIO_ReadPin(BOTON);	//Leo el valor del pulsador

		if(!estados_estables)
		{
			estado_anterior=estado_actual;
			estados_estables=1;
		}
		if(estados_estables==ESTADOS_ESTABLES)
		{
			estados_estables=ESTADOS_ESTABLES+1;
			flag_boton=estado_anterior;
			if(flag_boton)	//se presiono boton
			{
				estado++;
				estado%=CANT_ESTADOS+1;
				flag_boton=0;
//				vTaskResume(tarea_prior);	//Cuando se detecta un boton, se reanuda la tarea de manejo de prioridades y sigue su curso en la maquina de estados
			}
		}
		if(estado_actual==estado_anterior)
		{
			estados_estables++;
		}
		if(estado_actual!=estado_anterior)
		{
			estados_estables=0;
		}
		if(estados_estables>ESTADOS_ESTABLES)
		{
			estados_estables=ESTADOS_ESTABLES+1;
		}

		vTaskDelay(1/portTICK_RATE_MS);	//cada 1 ms consulta si se presiono el boton
	}

}
